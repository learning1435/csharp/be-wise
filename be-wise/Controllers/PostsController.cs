using System.Linq;
using be_wise.Models.Home.Dto;
using be_wise.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace be_wise.Controllers
{
    public class PostsController : Controller
    {
        private readonly PostsRepository _postsRepository;
        
        public PostsController(PostsRepository postsRepository)
        {
            this._postsRepository = postsRepository;
        }

        public IActionResult CategorisedPosts(int id, int page = 1)
        {
            var postsCollection = new PostCollectionDto();

            var totalItems = this._postsRepository.GetPostsForCategoryCount(id);
            var pagesCount = (totalItems / 20);
            
            postsCollection.CanPrevious = page > 1;
            postsCollection.CanNext = page < pagesCount;
            postsCollection.PagesCount = pagesCount > 0 ? pagesCount : 1;
            postsCollection.Page = page;
            postsCollection.NextPage = page + 1;
            postsCollection.PreviousPage = page - 1;
            
            postsCollection.Posts = this._postsRepository.GetPostsForCategory(id, 20, page)
                .Select(x => new PostDto
            {
                Id = x.Id,
                Title = x.Title,
                ImageUrl = x.ImagePath
            }).ToList();
            
            return View("CategorisedPosts", postsCollection);
        }

        public IActionResult NewestPosts(int page = 1)
        {
            var postsCollection = new PostCollectionDto();

            var totalItems = this._postsRepository.AcceptedPostsCount();
            var pagesCount = (totalItems / 20);
            
            postsCollection.CanPrevious = page > 1;
            postsCollection.CanNext = page < pagesCount;
            postsCollection.PagesCount = pagesCount > 0 ? pagesCount : 1;
            postsCollection.Page = page;
            postsCollection.NextPage = page + 1;
            postsCollection.PreviousPage = page - 1;
            
            postsCollection.Posts = this._postsRepository.GetNewestPosts(20).Select(x => new PostDto
            {
                Id = x.Id,
                Title = x.Title,
                ImageUrl = x.ImagePath
            }).ToList();
            
            return View("NewestPosts", postsCollection);
        }

        public IActionResult ShowPost(int id)
        {
            var post = this._postsRepository.GetPost(id);
            var postDto = new PostDto
            {
                Id = post.Id,
                Title = post.Title,
                ImageUrl = post.ImagePath
            };
            
            return View("ShowPost", postDto);
        }
    }
}