using System;
using System.Linq;
using be_wise.Models.Dashboard;
using be_wise.Models.Dashboard.Dto;
using be_wise.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace be_wise.Controllers
{
    [Authorize]
    public class DashboardController : Controller
    {
        private readonly UsersRepository _usersRepository;
        private readonly PostsRepository _postsRepository;
        
        public DashboardController(UsersRepository usersRepository, PostsRepository postsRepository)
        {
            this._usersRepository = usersRepository;
            this._postsRepository = postsRepository;
        }
        
        // GET
        public IActionResult Index()
        {
            var viewModel = new DashboardViewModel
            {
                UsersCount = this._usersRepository.UsersCount(),
                AllPostsCount = this._postsRepository.PostsCount(),
                PendingPostsCount = this._postsRepository.PendingPostsCount(),
                PostsInMonthCount = this._postsRepository.PostsInMonthCount(DateTime.Now),
                PendingPosts = this._postsRepository.GetPendingPosts().Select(x => new PostDto
                {
                    Id = x.Id,
                    Title = x.Title,
                    ImageUrl = x.ImagePath,
                    InsertedAt = x.InsertedAt,
                    IsPending = x.IsPending
                })
            };
            
            return View("Index", viewModel);
        }

        public IActionResult Accept(int id)
        {
            this._postsRepository.AcceptPost(id);
            
            return RedirectToAction("Index");
        }
        
        public IActionResult Reject(int id)
        {
            this._postsRepository.RejectPost(id);
            
            return RedirectToAction("Index");
        }
    }
}