﻿using System.Diagnostics;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using be_wise.Models;
using be_wise.Models.Home;
using be_wise.Models.Home.Dto;
using be_wise.Repositories;

namespace be_wise.Controllers
{
    public class HomeController : Controller
    {
        private readonly CategoriesRepository _categoriesRepository;
        private readonly PostsRepository _postsRepository;
        
        public HomeController(CategoriesRepository categoriesRepository, PostsRepository postsRepository)
        {
            this._categoriesRepository = categoriesRepository;
            this._postsRepository = postsRepository;
        }
        
        public IActionResult Index()
        {

            var viewModel = new HomeViewModel();            
            
            viewModel.Categories = this._categoriesRepository.GetCategories().Select(x => new CategoryDto
            {
                Id = x.Id,
                Title = x.Title,
                ImageUrl = x.Cover
            }).ToList();
            
            viewModel.NewestPosts = this._postsRepository.GetNewestPosts().Select(x => new PostDto
            {
                Id = x.Id,
                Title = x.Title,
                ImageUrl = x.ImagePath
            }).ToList();
            
            return View(viewModel);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }
    }
}