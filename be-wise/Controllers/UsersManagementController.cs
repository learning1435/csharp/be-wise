using System;
using System.Linq;
using be_wise.Models.Dashboard;
using be_wise.Models.Dashboard.Dto;
using be_wise.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace be_wise.Controllers
{
    public class UsersManagementController : Controller
    {
        private readonly UsersRepository _usersRepository;
        
        public UsersManagementController(UsersRepository usersRepository)
        {
            this._usersRepository = usersRepository;
        }

        public IActionResult Index()
        {
            var viewModel = new UserIndexViewModel();
            
            viewModel.Users = this._usersRepository.GetUsers().Select(x => new UserDto
            {
                Id = x.Id,
                Email = x.Email,
                Username = x.Username,
                IsAdmin = x.IsAdmin
            });
            
            return View("Index", viewModel);
        }

        public IActionResult AddPermission(int id)
        {
            this._usersRepository.SetAdmin(id);
            
            return RedirectToAction("Index");
        }
    }
}