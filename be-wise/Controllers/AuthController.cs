using System.Security.Claims;
using be_wise.Models.Auth;
using be_wise.Repositories;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;

namespace be_wise.Controllers
{
    public class AuthController : Controller
    {

        private readonly AuthRepository _authRepository;

        public AuthController(AuthRepository authRepository)
        {
            this._authRepository = authRepository;
        }
        
        // GET
        public IActionResult Login()
        {
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            
            return View("Login");
        }

        [HttpPost]
        public IActionResult Login(LoginViewModel credentials)
        {
            
            // TODO add some validation

            var claims = this._authRepository.Login(credentials);
            var login = HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, claims);
            
            return RedirectToAction("Index", "Home"); 
        }
        
        
        // GET
        public IActionResult Register()
        {
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            
            return View("Register");
        }

        [HttpPost]
        public IActionResult Register(RegisterViewModel registerData)
        {
            this._authRepository.Register(registerData);

            return RedirectToAction("Login");
        }

        public IActionResult Logout()
        {
            if (!HttpContext.User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            
            var login = HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);  
            return RedirectToAction("Login");  
        }
    }
}