using System.Linq;
using be_wise.Models.Dashboard;
using be_wise.Models.Dashboard.Dto;
using be_wise.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace be_wise.Controllers
{
    public class AdminsManagementController : Controller
    {

        private readonly UsersRepository _usersRepository;
        
        public AdminsManagementController(UsersRepository usersRepository)
        {
            this._usersRepository = usersRepository;
        }
        
        public IActionResult Index()
        {
            var viewModel = new UserIndexViewModel();
            
            viewModel.Users = this._usersRepository.GetAdmins().Select(x => new UserDto
            {
                Id = x.Id,
                Email = x.Email,
                Username = x.Username,
                IsAdmin = x.IsAdmin
            });
            
            return View("Index", viewModel);
        }
        
        public IActionResult RemovePermission(int id)
        {
            this._usersRepository.UnsetAdmin(id);
            
            return RedirectToAction("Index");
        }
    }
}