using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using be_wise.Models.Dashboard;
using be_wise.Models.Dashboard.Dto;
using be_wise.Repositories;
using Microsoft.AspNetCore.Mvc;
using PostDto = be_wise.Models.Home.Dto.PostDto;

namespace be_wise.Controllers
{
    public class PostsManagementController : Controller
    {
        private readonly PostsRepository _postsRepository;
        private readonly CategoriesRepository _categoriesRepository;
        private readonly UsersRepository _usersRepository;

        public PostsManagementController(PostsRepository postsRepository,
            UsersRepository usersRepository,
            CategoriesRepository categoriesRepository)
        {
            this._postsRepository = postsRepository;
            this._categoriesRepository = categoriesRepository;
            this._usersRepository = usersRepository;
        }

        public IActionResult Index()
        {
            var viewModel = new PostIndexViewModel();

            viewModel.Posts = new List<PostDto>();

            var categories = this._postsRepository.GetPosts().ToList();

            viewModel.Posts = categories.Select(x => new PostDto
            {
                Id = x.Id,
                Title = x.Title,
                ImageUrl = x.ImagePath
            });

            return View(viewModel);
        }

        public IActionResult Create()
        {
            var viewModel = new EditPostViewModel();
            viewModel.Categories = this._categoriesRepository.GetCategories().Select(x => new CategoryDto
            {
                Id = x.Id,
                Title = x.Title
            });

            return View("Create", viewModel);
        }

        [HttpPost]
        public IActionResult Create(EditPostViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    viewModel.Categories = this._categoriesRepository.GetCategories().Select(x => new CategoryDto
                    {
                        Id = x.Id,
                        Title = x.Title
                    });
                    return View("Create", viewModel);
                }

                var userEmail = HttpContext.User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Email).Value;
                var user = this._usersRepository.GetUserByEmail(userEmail);

                this._postsRepository.StorePost(viewModel, user);
            }
            catch (Exception e)
            {
                viewModel.ErrorMessage = e.Message;
                return View("Create", viewModel);
            }

            return RedirectToAction("Index");
        }

        public IActionResult Remove(int id)
        {
            try
            {
                this._postsRepository.DeletePost(id);

                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return RedirectToAction("Index");
            }
        }
    }
}