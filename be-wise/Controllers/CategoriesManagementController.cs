using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using be_wise.Models.Dashboard;
using be_wise.Models.Dashboard.Dto;
using be_wise.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace be_wise.Controllers
{
    public class CategoriesManagementController : Controller
    {
        private readonly CategoriesRepository _categoriesRepository;

        public CategoriesManagementController(CategoriesRepository categoriesRepository)
        {
            this._categoriesRepository = categoriesRepository;
        }

        public IActionResult Index()
        {
            var viewModel = new CategoryIndexViewModel();

            viewModel.Categories = new List<CategoryDto>();

            var categories = this._categoriesRepository.GetCategories().ToList();

            viewModel.Categories = categories.Select(x => new CategoryDto
            {
                Id = x.Id,
                Title = x.Title,
                ImageUrl = x.Cover
            });

            return View(viewModel);
        }

        public IActionResult Create()
        {
            return View("Create", new EditCategoryViewModel());
        }

        [HttpPost]
        public IActionResult Create(EditCategoryViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View("Create", viewModel);
                }

                this._categoriesRepository.StoreCategory(viewModel);
            }
            catch (Exception e)
            {
                viewModel.ErrorMessage = e.Message;
                return View("Create", viewModel);
            }

            return RedirectToAction("Index");
        }

        public IActionResult Edit(int id)
        {
            var viewModel = new EditCategoryViewModel();
            
            try
            {
                var category = this._categoriesRepository.GetCategoryById(id);

                if (category == null)
                {
                    return RedirectToAction("Index");
                }
                
                viewModel.Id = category.Id;
                viewModel.Title = category.Title;

                return View("Edit", viewModel);

            }
            catch (Exception e)
            {
                viewModel.ErrorMessage = e.Message;
                return View("Edit", viewModel);
            }
        }

        [HttpPost]
        public IActionResult Edit(EditCategoryViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View("Edit", viewModel);
                }

                this._categoriesRepository.UpdateCategory(viewModel);
            }
            catch (Exception e)
            {
                viewModel.ErrorMessage = e.Message;
                return View("Create", viewModel);
            }

            return RedirectToAction("Index");
        }

        public IActionResult Remove(int id)
        {
            try
            {
                this._categoriesRepository.DeleteCategory(id);

                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return RedirectToAction("Index");
            }
        }
    }
}