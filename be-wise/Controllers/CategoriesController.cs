using System.Linq;
using be_wise.Models.Home.Dto;
using be_wise.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace be_wise.Controllers
{
    public class CategoriesController : Controller
    {
        private readonly CategoriesRepository _categoriesRepository;

        public CategoriesController(CategoriesRepository categoriesRepository)
        {
            this._categoriesRepository = categoriesRepository;
        }
        
        public IActionResult Index(int categoryId)
        {
            var categories = this._categoriesRepository.GetCategories().Select(x => new CategoryDto
            {
                Id = x.Id,
                Title = x.Title,
                ImageUrl = x.Cover
            }).ToList();
            
            return View("Index", categories);
        }
    }
}