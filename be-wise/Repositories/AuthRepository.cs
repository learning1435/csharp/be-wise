using System;
using System.Linq;
using System.Security.Claims;
using be_wise.Data;
using be_wise.Data.Models;
using be_wise.Models.Auth;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace be_wise.Repositories
{
    public class AuthRepository
    {
        private readonly ApplicationDbContext _dbContext;

        public AuthRepository(ApplicationDbContext dbContext)
        {
            this._dbContext = dbContext;
        }
        
        public ClaimsPrincipal Login(LoginViewModel credentials)
        {
            var user = this._dbContext.Users.FirstOrDefault(x => x.Username == credentials.Username);

            if (user == null)
            {
                throw new Exception("Użytkownik nie istnieje!");
            }

            if (user.Password == credentials.Password)
            {
                //Create the identity for the user  
                var identity = new ClaimsIdentity(new[] {  
                    new Claim(ClaimTypes.Email, user.Email),  
                    new Claim(ClaimTypes.Role, user.IsAdmin ? "admin" : "user"),
                    new Claim(ClaimTypes.Name, user.Username)
                }, CookieAuthenticationDefaults.AuthenticationScheme);  
  
                return new ClaimsPrincipal(identity);  
            }
            
            throw new Exception("Nieprawidłowe hasło!");
        }

        public void Register(RegisterViewModel registerData)
        {
            var newUser = new User
            {
                Email = registerData.Email,
                Username = registerData.Username,
                Password = registerData.Password,
                IsAdmin = false
            };

            this._dbContext.Users.Add(newUser);
            this._dbContext.SaveChanges();
        }
    }
}