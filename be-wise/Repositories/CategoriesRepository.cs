using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using be_wise.Data;
using be_wise.Data.Models;
using be_wise.Models.Dashboard.Dto;

namespace be_wise.Repositories
{
    public class CategoriesRepository
    {
        private readonly ApplicationDbContext _dbContext;

        public CategoriesRepository(ApplicationDbContext dbContext)
        {
            this._dbContext = dbContext;
        }

        public IEnumerable<Category> GetCategories()
        {
            return this._dbContext.Categories.ToList();
        }

        public void CreateOrUpdateCategory(Category category)
        {
            if (category.Id == 0)
                this._dbContext.Categories.Add(category);
            else
                this._dbContext.Categories.Update(category);

            this._dbContext.SaveChanges();
        }

        public Category GetCategoryById(int id)
        {
            return this._dbContext.Categories.FirstOrDefault(x => x.Id == id);
        }

        public void DeleteCategory(int id)
        {
            var category = this._dbContext.Categories.FirstOrDefault(x => x.Id == id);
            if (category != null)
                this.DeleteCategory(category);
        }

        public void DeleteCategory(Category category)
        {
            if (category.Id == 0) return;

            this._dbContext.Categories.Remove(category);

            var posts = this._dbContext.Posts.Where(x => x.CategoryId == category.Id).Select(x => x);
            this._dbContext.Posts.RemoveRange(posts);

            this._dbContext.SaveChanges();
        }

        public void StoreCategory(EditCategoryViewModel category)
        {
            if (category.Image == null || category.Image.Length == 0)
                throw new Exception("Brak obrazu");

            var relativePath = Path.Combine("\\stored", "categories", category.Image.FileName);
            var path = Path.Combine(
                Directory.GetCurrentDirectory(), "wwwroot", "stored", "categories",
                category.Image.FileName);

            using (var stream = new FileStream(path, FileMode.Create))
            {
                category.Image.CopyTo(stream);
            }

            var newCategory = new Category
            {
                Cover = relativePath,
                Title = category.Title
            };

            this.CreateOrUpdateCategory(newCategory);
        }

        public void UpdateCategory(EditCategoryViewModel category)
        {
            if (category.Image == null || category.Image.Length == 0)
                throw new Exception("Brak obrazu");

            var relativePath = Path.Combine("\\stored", "categories", category.Image.FileName);
            var path = Path.Combine(
                Directory.GetCurrentDirectory(), "wwwroot", "stored", "categories",
                category.Image.FileName);

            using (var stream = new FileStream(path, FileMode.Create))
            {
                category.Image.CopyTo(stream);
            }

            var newCategory = new Category
            {
                Cover = relativePath,
                Title = category.Title
            };

            if (category.Id.HasValue)
            {
                newCategory.Id = category.Id.Value;
            }

            this.CreateOrUpdateCategory(newCategory);
        }
    }
}