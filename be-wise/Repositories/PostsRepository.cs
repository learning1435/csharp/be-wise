using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using be_wise.Data;
using be_wise.Data.Models;
using be_wise.Models.Dashboard.Dto;

namespace be_wise.Repositories
{
    public class PostsRepository
    {
        private readonly ApplicationDbContext _dbContext;

        public PostsRepository(ApplicationDbContext dbContext)
        {
            this._dbContext = dbContext;
        }

        public int PostsCount()
        {
            return this._dbContext.Posts.Count();
        }

        public int AcceptedPostsCount()
        {
            return this._dbContext.Posts.Count(x => x.IsPending == false);
        }

        public int PendingPostsCount()
        {
            return this._dbContext.Posts.Count(x => x.IsPending == true);
        }

        public IEnumerable<Post> GetPendingPosts()
        {
            return this._dbContext.Posts.Where(x => x.IsPending == true).Select(x => x);
        }

        public int PostsInMonthCount(DateTime monthTime)
        {
            return this._dbContext.Posts
                .Count(x => x.InsertedAt.Year == monthTime.Year && x.InsertedAt.Month == monthTime.Month);
        }

        public Post GetPost(int id)
        {
            return this._dbContext.Posts.FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<Post> GetPosts()
        {
            return this._dbContext.Posts.Select(x => x);
        }

        public int GetPostsForCategoryCount(int categoryId)
        {
            return this._dbContext.Posts
                .Where(x => x.IsPending == false)
                .Count(x => x.CategoryId == categoryId);
        }

        public IEnumerable<Post> GetPostsForCategory(int categoryId, int perPage = 20, int page = 1)
        {
            return this._dbContext.Posts
                .Where(x => x.CategoryId == categoryId)
                .Where(x => x.IsPending == false)
                .OrderBy(x => x.InsertedAt)
                .Skip((page - 1) * perPage)
                .Take(perPage)
                .ToList();
        }

        public IEnumerable<Post> GetAcceptedPosts()
        {
            return this._dbContext.Posts.Where(x => x.IsPending == false).Select(x => x);
        }

        public IEnumerable<Post> GetPostsForUser(User user)
        {
            return this._dbContext.Posts.Where(x => x.UserId == user.Id).Select(x => x);
        }

        public IEnumerable<Post> GetBestPosts()
        {
            return this._dbContext.Posts.Take(5).ToList();
        }

        public IEnumerable<Post> GetNewestPosts(int perPage = 20, int page = 1)
        {
            return this._dbContext.Posts
                .Where(x => x.IsPending == false)
                .OrderBy(x => x.InsertedAt)
                .Skip((page - 1) * perPage)
                .Take(perPage)
                .ToList();
        }

        public void DeletePost(int id)
        {
            var post = this._dbContext.Posts.FirstOrDefault(x => x.Id == id);
            if (post != null)
                this.DeletePost(post);
        }

        public void DeletePost(Post post)
        {
            if (post.Id == 0) return;

            this._dbContext.Posts.Remove(post);
            this._dbContext.SaveChanges();
        }

        public void StorePost(EditPostViewModel post, User user)
        {
            if (post.Image == null || post.Image.Length == 0)
                throw new Exception("Obraz jest wymagany!");

            var relativePath = Path.Combine("\\stored", "posts", post.Image.FileName);
            var path = Path.Combine(
                Directory.GetCurrentDirectory(), "wwwroot", "stored", "posts",
                post.Image.FileName);

            using (var stream = new FileStream(path, FileMode.Create))
            {
                post.Image.CopyTo(stream);
            }

            var newPost = new Post
            {
                ImagePath = relativePath,
                InsertedAt = DateTime.Now,
                UserId = user.Id,
                Title = post.Content,
                CategoryId = post.CategoryId,
                IsPending = true
            };

            this._dbContext.Posts.Add(newPost);
            this._dbContext.SaveChanges();
        }

        public void AcceptPost(int id)
        {
            var post = this.GetPost(id);
            if (post == null)
                return;

            post.IsPending = false;
            this._dbContext.Posts.Update(post);
            this._dbContext.SaveChanges();
        }

        public void RejectPost(int id)
        {
            this.DeletePost(id);
        }
    }
}