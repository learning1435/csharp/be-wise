using System.Collections.Generic;
using System.Linq;
using be_wise.Data;
using be_wise.Data.Models;

namespace be_wise.Repositories
{
    public class UsersRepository
    {
        private readonly ApplicationDbContext _dbContext;

        public UsersRepository(ApplicationDbContext dbContext)
        {
            this._dbContext = dbContext;
        }

        public int UsersCount()
        {
            return this._dbContext.Users.Count();
        }
        
        public User GetUser(int id)
        {
            return this._dbContext.Users.FirstOrDefault(x => x.Id == id);
        }

        public User GetUserByEmail(string email)
        {
            return this._dbContext.Users.FirstOrDefault(x => x.Email == email);
        }
        
        public IEnumerable<User> GetUsers()
        {
            return this._dbContext.Users.Select(x => x);
        }

        public IEnumerable<User> GetAdmins()
        {
            return this._dbContext.Users.Where(x => x.IsAdmin == true).Select(x => x);
        }

        public void SetAdmin(int id)
        {
            var user = this.GetUser(id);
            if (user == null)
                return;

            user.IsAdmin = true;
            this._dbContext.Users.Update(user);
            this._dbContext.SaveChanges();
        }

        public void UnsetAdmin(int id)
        {
            var user = this.GetUser(id);
            if (user == null)
                return;

            user.IsAdmin = false;
            this._dbContext.Users.Update(user);
            this._dbContext.SaveChanges(); 
        }
    }
}