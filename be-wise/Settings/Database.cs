using Microsoft.Extensions.Configuration;

namespace be_wise.Settings
{
    public class Database : ConfigurationSection
    {
        public string ConnectionString { get; set; }

        public Database(ConfigurationRoot root, string path) : base(root, path)
        {
        }
    }
}