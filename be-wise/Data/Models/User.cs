using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace be_wise.Data.Models
{
    
    [Table("Users")]
    public class User
    {
        [Column("Id")]
        public int Id { get; set; }
        
        [Column("Username")]
        public string Username { get; set; }

        [Column("Email")]
        public string Email { get; set; }
        public string Password { get; set; }
        
        [DefaultValue(false)]
        public bool IsAdmin { get; set; }
    }
}