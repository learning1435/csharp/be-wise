using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace be_wise.Data.Models
{
    [Table("Categories")]
    public class Category
    {
        [DefaultValue(0)]
        [Column("Id")]
        public int Id { get; set; }
        
        [Column("Title")]
        public string Title { get; set; }
        
        [Column("Cover")]
        public string Cover { get; set; }
    }
}