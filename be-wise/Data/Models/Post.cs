using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace be_wise.Data.Models
{
    [Table("Posts")]
    public class Post
    {
        [Column("Id")] 
        public int Id { get; set; }
        
        [ForeignKey("Owner")]
        public int UserId { get; set; }
        
        [Column("Title")]
        public string Title { get; set; }
        
        [Column("ImagePath")]
        public string ImagePath { get; set; }
        
        [Column("InsertedAt")]
        public DateTime InsertedAt { get; set; }
        
        [Column("Pending")]
        public bool IsPending { get; set; }
        
        [Column("CategoryId")]
        public int CategoryId { get; set; }
    }
}