using System.Collections.Generic;

namespace be_wise.Models.Home.Dto
{
    public class PostCollectionDto
    {
        public List<PostDto> Posts { get; set; }

        public bool CanPrevious { get; set; }

        public bool CanNext { get; set; }

        public int PagesCount { get; set; }
        
        public int Page { get; set; }
        
        public int NextPage { get; set; }
        
        public int PreviousPage { get; set; }
    }
}