using System.Collections.Generic;
using be_wise.Models.Home.Dto;

namespace be_wise.Models.Home
{
    public class HomeViewModel
    {
        public List<CategoryDto> Categories { get; set; }
        public List<PostDto> NewestPosts { get; set; }
        public List<PostDto> BestPosts { get; set; }
    }
}