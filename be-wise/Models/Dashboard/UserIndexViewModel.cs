using System.Collections.Generic;
using be_wise.Models.Dashboard.Dto;

namespace be_wise.Models.Dashboard
{
    public class UserIndexViewModel
    {
        public IEnumerable<UserDto> Users { get; set; }
    }
}