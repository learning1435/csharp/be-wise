using System;

namespace be_wise.Models.Dashboard.Dto
{
    public class PostDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string ImageUrl { get; set; }
        public DateTime InsertedAt { get; set; }
        public bool IsPending { get; set; }
    }
}