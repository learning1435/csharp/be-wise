using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;
using Microsoft.AspNetCore.Http;

namespace be_wise.Models.Dashboard.Dto
{
    public class EditPostViewModel
    {
        public int Id { get; set; }
        
        [Required(ErrorMessage = "Tytuł jest wymagany!")]
        public string Content { get; set; }
        
        public int OwnerId { get; set; }
        
        [Required(ErrorMessage = "Kategoria jest wymagana!")]
        public int CategoryId { get; set; }

        [Required(ErrorMessage = "Obraz jest wymagany!")]
        public IFormFile Image { get; set; }
        
        [FileExtensions(Extensions = "png,jpg,jpeg", ErrorMessage = "Nieprawidłowe rozszerzenie obrazy (png, jpg, gif)!")]
        public string FileName
        {
            get
            {
                if (Image != null)
                    return Image.FileName;
                else
                    return "";
            }
        }

        public IEnumerable<CategoryDto> Categories { get; set; }

        public string ErrorMessage { get; set; }
    }
}