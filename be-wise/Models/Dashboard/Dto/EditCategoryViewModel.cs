using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace be_wise.Models.Dashboard.Dto
{
    public class EditCategoryViewModel
    {
        [DefaultValue(null)]
        public int? Id { get; set; }
        
        [Required(ErrorMessage = "Kategoria jest wymagana!")]
        public string Title { get; set; }
        
        [Required(ErrorMessage = "Obraz jest wymagany!")]
        public IFormFile Image { get; set; }
        
        [FileExtensions(Extensions = "png,jpg,jpeg", ErrorMessage = "Nieprawidłowe rozszerzenie obrazy (png, jpg, gif)!")]
        public string FileName
        {
            get
            {
                if (Image != null)
                    return Image.FileName;
                else
                    return "";
            }
        }
        
        public string ErrorMessage { get; set; }
    }
}