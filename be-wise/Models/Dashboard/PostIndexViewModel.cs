using System.Collections.Generic;
using be_wise.Models.Home.Dto;

namespace be_wise.Models.Dashboard
{
    public class PostIndexViewModel
    {
        public IEnumerable<PostDto> Posts { get; set; }
        public int Pages { get; set; }
        public int Entries { get; set; }
        public int ShowEntries { get; set; }
    }
}