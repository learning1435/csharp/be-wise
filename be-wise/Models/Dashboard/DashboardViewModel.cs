using System.Collections.Generic;
using be_wise.Models.Dashboard.Dto;

namespace be_wise.Models.Dashboard
{
    public class DashboardViewModel
    {
        public int PostsInMonthCount { get; set; }
        public int AllPostsCount { get; set; }
        public int UsersCount { get; set; }
        public int PendingPostsCount { get; set; }
        public IEnumerable<PostDto> PendingPosts { get; set; }
    }
}