using System.Collections.Generic;
using be_wise.Models.Dashboard.Dto;

namespace be_wise.Models.Dashboard
{
    public class CategoryIndexViewModel
    {
        public IEnumerable<CategoryDto> Categories { get; set; }
        public int Pages { get; set; }
        public int Entries { get; set; }
        public int ShowEntries { get; set; }
    }
}